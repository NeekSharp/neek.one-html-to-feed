﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CommonLibrary;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("HtmlToFeed.Test")]

namespace HtmlToFeed
{
    public class Html2Feed
    {
        private static readonly Regex _negativHtmlTags = new Regex(@"footer|\bhead\b|nav|form|aside|ul|li", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _positivHtmlTags = new Regex("div|article|section|li|main|ul", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _hTags = new Regex("h1|h2|h3", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex _positivUriWordsForCategory = new Regex("category|categories|\bcat=|tag[s]?|label|hub|topic", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _negativeAttributes = new Regex("footer|slider|banner|icon|comment|combx|community|disqus|extra|foot|remark|rss|shoutbox|sidebar|sponsor|ad-break|agegate|pagination|pager|popup|tweet|twitter|related|promo", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _positiveAttributes = new Regex("post|title|preview|article", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _negativeWordsOnLink = new Regex(@"comment|favorite|tag|category|\/pdf\b|\be[-]?mail\b|author|\bcat\b|\bsite\b", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _UnlikelyWordsInAttributes = new Regex("footer|banner|\bmenu\b|comment|\btags\b|categories", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _htmlTagsForRemoving = new Regex(@"\bhead\b|aside|nav|footer|style|hr|br|script|noscript|form", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _UnlikelyElements = new Regex("nav|footer|noscript|script", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _BadLinks = new Regex(@"&[\w]+;", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private static readonly Regex _veryNegativUriWordsForPostTitle = new Regex(@"author|comments|comment|feed|page|forum|subscrib", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

        private Uri _baseHost = null;

        private HtmlDocument _html = null;

        private Dictionary<string, int> _internalLinks;

        private Dictionary<string,int> InternalLinks
        {
            get
            {
                if(_internalLinks == null)
                {
                    _internalLinks = InternalLinksListCount(_html);
                }

                return _internalLinks;
            }
        }

        public Html2Feed(Uri host, string html)
        {
            _html = new HtmlDocument();
            _html.LoadHtml(html);
            _baseHost = host;
        }


        public List<FeedItem> ListElements()
        {
            RemoveUnlikelyElements();

            List<HtmlNode> nodes = SimilarElements();

            if(nodes.Count < 2)
            {
                nodes = ArticlesElements();
            }

            if (nodes.Count < 2)
            {
                nodes = LagestListElements();
            }

            return FeedItemsFromHtml(nodes);
        }

        /// <summary>
        /// Ищем на странице элементы, похожие на ссылки постов блога
        /// </summary>
        /// <returns>Элементы с постами</returns>
        /// 
        public List<HtmlNode> SimilarElements()
        {
            // Ищем элемент-контейнер, где наиболее вероятно содержатся элементы списка статей
            Dictionary<HtmlNode, Dictionary<HtmlNode, int>> nodes = FindRecurringElementsContainers(_html);

            // Элемент-контейнер набравший больше всего баллов
            HtmlNode vinner = null;

            // Элементы контейнера победителя
            List<HtmlNode> vinnerItems = new List<HtmlNode>();

            // Минимальный балл который должен иметь элемент-контенер для участия в выборе
            double bestAvgScore = 14F;
            foreach (KeyValuePair<HtmlNode, Dictionary<HtmlNode, int>> node in nodes)
            {
                double sum = node.Value.Sum(a => a.Value);
                double max = node.Value.Max(a => a.Value);

                if (bestAvgScore < sum)
                {
                    vinner = node.Key;
                    vinnerItems = node.Value.Where(a => a.Value > 14).Select(a => a.Key).ToList();

                    bestAvgScore = sum;
                }

            }

            return vinnerItems;

        }

        public List<HtmlNode> ArticlesElements()
        {
            return _html.DocumentNode.Descendants("article").ToList();
        }

        public List<HtmlNode> LagestListElements() {

            var ulList = _html.DocumentNode.Descendants("ul").ToList();
            
            int maxLength = 0;
            HtmlNode biggestNode = null;
            foreach (HtmlNode node in ulList)
            {
                int length = node.InnerText.Length;
                if (length > maxLength)
                {
                    maxLength = length;
                    biggestNode = node;
                }
            }

            return biggestNode != null ? biggestNode.ChildNodes.ToList() : new List<HtmlNode>();
        }

        public Dictionary<string,int> InternalLinksListCount(HtmlDocument html)
        {
            HtmlNodeCollection ahrefs = html.DocumentNode.SelectNodes(@"//a[@href]");

            Dictionary<string, int> internalLinks = new Dictionary<string, int>();

            if (ahrefs == null || ahrefs.Count == 0)
                return internalLinks;

            foreach (HtmlNode link in ahrefs)
            {
                string href = link.GetAttributeValue("href", "#").Trim();

                if (internalLinks.ContainsKey(href))
                {
                    internalLinks[href]++;
                }
                else
                {
                    internalLinks.Add(href, 1);
                }
            }

            return internalLinks;
        }

        /// <summary>
        /// Поиск элементов-контейнеров со списком статей
        /// </summary>
        /// <param name="doc">HtmlDocument <see cref="HtmlAgilityPack.HtmlDocument"/></param>
        /// <returns>Словарь элементов-контейнеров и вложенных элементов</returns>
        internal Dictionary<HtmlNode, Dictionary<HtmlNode, int>> FindRecurringElementsContainers(HtmlDocument doc)
        {
            Dictionary<HtmlNode, Dictionary<HtmlNode, int>> recurringElements = new Dictionary<HtmlNode, Dictionary<HtmlNode, int>>();

            List<HtmlNode> nn = doc.DocumentNode.Descendants().Where(a => _positivHtmlTags.IsMatch(a.Name) && !a.Name.Equals("li") && a.HasChildNodes && a.ChildNodes.Where(b => b.NodeType == HtmlNodeType.Element).Count() > 2).ToList();

            foreach (HtmlNode node in nn)
            {
                if (!HasUnlikelyWordsInAttributes(node))
                {
                    int childsCount = node.ChildNodes.Where(a => a.NodeType == HtmlNodeType.Element && !HasUnlikelyWordsInAttributes(a)).Count();

                    int childNodesWithLinksCount = 0;
                    
                    Dictionary<HtmlNode,int> childNodes = new Dictionary<HtmlNode, int>();
                    foreach (HtmlNode subNode in node.ChildNodes.Where(a => _positivHtmlTags.IsMatch(a.Name) || a.Name.Equals("a", StringComparison.InvariantCultureIgnoreCase)))
                    {
                        if (subNode.Name == "a")
                        {
                            if (subNode.Descendants("h1") != null || subNode.Descendants("h2") != null || subNode.Descendants("h3") != null)
                            {
                                childNodesWithLinksCount++;
                                KeyValuePair<HtmlNode, int> x = EstimateHtmlHode(subNode);
                                childNodes.Add(x.Key,x.Value);
                                
                            }
                        }
                        else if (subNode.Descendants("a") != null)
                        {
                            childNodesWithLinksCount++;
                            KeyValuePair<HtmlNode, int> x = EstimateHtmlHode(subNode);
                            childNodes.Add(x.Key, x.Value);
                        }
                    }

                    if (childNodesWithLinksCount != 0 && ((double)childNodesWithLinksCount / (double)childsCount) > 0.7)
                    {
                        recurringElements.Add(node, childNodes);
                    }
                }
            }

            return recurringElements;
        }

        internal bool HasUnlikelyWordsInAttributes(HtmlNode node)
        {
            if (node.HasAttributes)
            {
                StringBuilder attributes = new StringBuilder();
                foreach (HtmlAttribute attr in node.Attributes)
                {
                    attributes.Append(attr.Value);
                    attributes.Append(" ");
                }

                return _negativeAttributes.IsMatch(attributes.ToString());
            }
            return false;
        }


        /// <summary>
        /// Удаление элементов, которые нам точно не нужны, а только мешают
        /// </summary>
        /// <param name="page"></param>
        public void RemoveUnlikelyElements()
        {

            List<HtmlNode> nodesToRemove = new List<HtmlNode>();

            nodesToRemove.AddRange(
                _html.DocumentNode.Descendants()
                .Where(node => node.NodeType == HtmlNodeType.Comment)
                .ToList());

            nodesToRemove.AddRange(_html.DocumentNode.Descendants("hr").ToList());

            nodesToRemove.AddRange(_html.DocumentNode.Descendants("br").ToList());

            nodesToRemove.AddRange(
                _html.DocumentNode.Descendants()
                .Where(a => _htmlTagsForRemoving.IsMatch(a.Name))
                .ToList());

            nodesToRemove.AddRange(
                _html.DocumentNode.Descendants("a")
                .Where(a => String.IsNullOrWhiteSpace(a.InnerText) || a.GetAttributeValue("href", "").Equals("/"))
                .ToList());

            nodesToRemove.AddRange(
               _html.DocumentNode.Descendants("div")
               .Where(a => a.GetAttributeValue("class", "").Contains("footer") || a.Id.Contains("footer"))
               .ToList());

            RemoveWithChilds(nodesToRemove);

        }

        private void RemoveWithChilds(List<HtmlNode> nodes)
        {
            foreach (HtmlNode node in nodes)
            {
                //node.Descendants().ToList().ForEach(a => a.Remove());
                node.InnerHtml = String.Empty;
                node.Remove();
            }
        }

   
        /// <summary>
        /// Оценка каждого элемента, с присвоением баллов
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private KeyValuePair<HtmlNode, int> EstimateHtmlHode(HtmlNode node)
        {
            int score = 0;
            score += СontainsImg(node);
            score += СontainsHTags(node);
            score += СontainsAuthorLink(node);
            score += LinkBeforeText(node);
            score += СontainsText(node);
            score += UnwantedAttributes(node);
            score += ArticlesTitleInHref(node);
            score += HaveBrothers(node);


            return new KeyValuePair<HtmlNode,int>(node, score);
        }

        // Методы для вычиления оценки страницы которая используется для их отбора
        // Positive
        internal int СontainsImg(HtmlNode node)
        {
            if(node.Descendants().Any(a => a.Name.Equals("img", StringComparison.InvariantCultureIgnoreCase)))
            {
                return 4;
            }

            return 0;
        }

        internal int HaveBrothers(HtmlNode node)
        {
            if (node.ParentNode.ChildNodes.Any(
                    a=>a.Name.Equals(node.Name,StringComparison.InvariantCultureIgnoreCase) 
                    && a.ChildNodes.Count == node.ChildNodes.Count 
                    && a.GetAttributeValue("class", "").Split().Intersect(node.GetAttributeValue("class", "").Split()).Count() != 0
                ) == true) 
            {
                return 5;
            }
            return 0;
        }

        internal int СontainsHTags(HtmlNode node)
        {
            IEnumerable<HtmlNode> hNodes = node.Descendants().Where(a => _hTags.IsMatch(a.Name));
            if (hNodes == null)
            {
                return 0;
            }
            else if (hNodes.Where(a => a.Name == "h1") != null){
                return 4;
            }
            else if (hNodes.Where(a => a.Name == "h2") != null){
                return 6;
            }
            else if (hNodes.Where(a => a.Name == "h3") != null){
                return 2;
            }
            else
            {
                return -3;
            }
        }

        internal int СontainsAuthorLink(HtmlNode node)
        {
            HashSet<string> authorKeys = new HashSet<string>(){ "author","user","member" };
            bool aLinks = node.Descendants("a").Any(a => a.HasAttributes && authorKeys.Contains(a.GetAttributeValue("href", "")));
            if (aLinks)
            {
               return 10;
            }
            return 0;
        }

        internal int LinkBeforeText(HtmlNode node)
        {
            HtmlNode aElement = node.SelectSingleNode("//a[@href]");
            HtmlNode pElement = node.SelectSingleNode("//p");

            if((pElement != null && aElement!= null) && (aElement.StreamPosition < pElement.StreamPosition))
            {
                return 5;
            }
            return 0;
        }

        internal int СontainsText(HtmlNode node)
        {
            if(node.InnerText.Length > 300)
            {
                return 10;
            }
            else
            {
                return -20;
            }
        }

        internal int PositiveAttributes(HtmlNode node)
        {
            if(node.HasAttributes && node.Attributes.Any(a=> _positiveAttributes.IsMatch(a.Value)))
            {
                return 3;
            }
            return 0;
        }

        // Negative
        internal int UnwantedAttributes(HtmlNode node)
        {
            if (node.HasAttributes && node.Attributes.Any(a => _negativeAttributes.IsMatch(a.Value)))
            {
                return 5;
            }
            return 0;
        }

        internal int ArticlesTitleInHref(HtmlNode node)
        {
            if(node.Descendants("a").Any(a => a.HasAttributes && a.GetAttributeValue("href", "").Count(c => c == '-') > 5))
            {
                return 5;
            }
            return 0;
        }

        internal int ExternalLinks(HtmlNode node)
        {
            //TODO: Проверку на домен
            if (node.Descendants("a").Any(a => a.HasAttributes && a.GetAttributeValue("href", "").StartsWith("http://")))
            {
                return -5;
            }
            return 0;
        }

        /// <summary>
        /// Получаем данные из предполагаемых элементов списка статей
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        internal List<FeedItem> FeedItemsFromHtml(List<HtmlNode> nodes)
        {
            List<FeedItem> posts = new List<FeedItem>();
            if (nodes.Count < 2) return posts;

            if (TryFindLink(nodes, out posts))
            {
                for(int i = 0; i < posts.Count; i++)
                {
                    SummaryText(posts[i]);
                }
            }

            return posts;
        }

        internal void SummaryText(FeedItem post)
        {
            string summary = SummaryTextFromSeveralNodes(post.Html);

            post.SummaryText = summary;
        }


        public string SummaryTextFromSeveralNodes(HtmlNode post)
        {
            int minInnerTextlengh = 150;
            int maxInnerTextlengh = 0;
            string result = null;

            //TODO: Надо как-то отсеивать внутренние ссылки и лишнее
            foreach(var node in post.ChildNodes)
            {
                string textWithoutDoubleSpaces = Regex.Replace(node.InnerText, @"\s+", " ");
                int currentNodeInnerTextLength = textWithoutDoubleSpaces.Length;

                if (currentNodeInnerTextLength > minInnerTextlengh && currentNodeInnerTextLength > maxInnerTextlengh)
                {
                    maxInnerTextlengh = currentNodeInnerTextLength;
                    result = textWithoutDoubleSpaces;
                }
            }

            return result;
        }


        /// <summary>
        /// Поиск ссылок на статьи в каждом элементе
        /// </summary>
        /// <param name="nodes">HtmlNode с полем количества баллов</param>
        /// <param name="items">Коллекция элементов статей с уже выделенными данными</param>
        /// <returns></returns>
        internal bool TryFindLink(List<HtmlNode> nodes, out List<FeedItem> items)
        {
            items = new List<FeedItem>();

            for (int i = 0; i < nodes.Count(); i++)
            {
                FeedItem postlink = new FeedItem();

                if (TryFindByHTags(nodes[i], postlink))
                {
                    items.Add(postlink);
                }
            }

            if (items.Count < 2)
            {
                items = ImplicitLinks(nodes);

                if(items.Count > 1)
                {
                    return true;
                }
            }
            else
            {
                return true;
            }

            return false;
        }


        /// <summary>
        /// Ищем ссылки, которые находятся в или покрывают тэги H
        /// </summary>
        /// <param name="node"></param>
        /// <param name="post"></param>
        /// <returns></returns>
        internal bool TryFindByHTags(HtmlNode node, FeedItem post)
        {
            if (node.Name.Equals("a") && FindTitleIfContainerEqLink(node, post))
            {
                return true;
            }
            else if (HIncludeA(node, post) || AIncludeH(node, post))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal bool FindTitleIfContainerEqLink(HtmlNode node, FeedItem post)
        {
            string href = node.GetAttributeValue("href", String.Empty);
            if (String.IsNullOrWhiteSpace(href)) return false;

            for (int i = 1; i < 5; i++)
            {
                HtmlNode titleNode = node.Descendants("h" + i).FirstOrDefault();
                if (titleNode != null && !String.IsNullOrWhiteSpace(titleNode.InnerText))
                {
                    Link link = AbsoluteUriFromHref(href, titleNode.InnerText);
                    if (link != null)
                    {
                        post.Link = link;
                        post.Html = node;

                        return true;
                    }
                }
            }

            HtmlNode elWithTitleAttribute = node.Descendants().FirstOrDefault(a => a.Attributes.Any(b => b.Value.Contains("title")) && !String.IsNullOrWhiteSpace(a.InnerText));
            if (elWithTitleAttribute != null)
            {
                post.Link = AbsoluteUriFromHref(href, elWithTitleAttribute.InnerText);
                post.Html = node;

                return true;
            }
            return false;
        }

        internal bool HIncludeA(HtmlNode node, FeedItem post)
        {
            for (int i = 1; i < 5; i++)
            {
                HtmlNode hh = node.Descendants("h" + i).FirstOrDefault(a => a.Descendants("a").Count() > 0);
                if (hh != null)
                {
                    List<HtmlNode> aNode = hh.Descendants("a").Where(a => a.GetAttributeValue("href", String.Empty) != String.Empty).ToList();
                    Dictionary<string,string> links = new Dictionary<string, string>();
                    foreach (HtmlNode a in aNode)
                    {
                        string href = a.GetAttributeValue("href", String.Empty).Trim();
                        string title = a.InnerText.Trim();

                        if (String.IsNullOrWhiteSpace(title) || String.IsNullOrWhiteSpace(href))
                        {
                            return false;
                        }

                        links.Add(href, title);
                    }

                    if (links.Count > 0)
                    {
                        string url = MostRareLink(links);
                        Link link = AbsoluteUriFromHref(url, links[url]);
                        if (link != null)
                        {
                            post.Html = node;
                            post.Link = link;

                            return true;
                        }

                    }
                }
            }
            return false;
        }

        private string MostRareLink(Dictionary<string, string> links)
        {
            return InternalLinks.Where(a => links.ContainsKey(a.Key)).OrderBy(a=>a.Value).First().Key;
        }

        internal Link AbsoluteUriFromHref(string href, string title)
        {
            Uri uri = null;
            Link link = null;

            if (href != String.Empty && !String.IsNullOrEmpty(title) && Uri.TryCreate(href, UriKind.RelativeOrAbsolute, out uri))
            {
                if (uri.IsAbsoluteUri)
                {
                    link = new Link(uri.AbsoluteUri, title.Trim());
                }
                else
                {
                    link = new Link(_baseHost, uri, title.Trim());
                }
            }
            return link;
        }

        internal bool AIncludeH(HtmlNode node, FeedItem post)
        {
            
            for (int i = 1; i < 5; i++)
            {
                List<HtmlNode> aNode = node.Descendants("a").Where(a => a.InnerHtml.Contains("h" + i)).ToList();
                Dictionary<string, string> links = new Dictionary<string, string>();
                foreach (HtmlNode a in aNode)
                {
                    string href = a.GetAttributeValue("href", String.Empty).Trim();
                    string title = a.InnerText.Trim();

                    if (String.IsNullOrWhiteSpace(title) || String.IsNullOrWhiteSpace(href))
                    {
                        return false;
                    }

                    links.Add(href, title);
                }

                if (links.Count > 0)
                {
                    string url = MostRareLink(links);
                    Link link = AbsoluteUriFromHref(url, links[url]);
                    if (link != null)
                    {
                        post.Html = node;
                        post.Link = link;

                        return true;
                    }

                }
            }
            return false;
        }

        internal List<FeedItem> ImplicitLinks(List<HtmlNode> posts)
        {
            Dictionary<HtmlNode, List<Link>> postsLinks = new Dictionary<HtmlNode, List<Link>>();

            Dictionary<string, int> hrefsOccurrence = new Dictionary<string, int>();
            Dictionary<string, int> titlesOccurrence = new Dictionary<string, int>();

            // Собираем все ссылки 
            for (int i = 0; i < posts.Count(); i++)
            {
                List<Link> linksFromOnePost = LinkListFromPost(posts[i]);

                foreach(var oneLink in linksFromOnePost)
                {
                    if (hrefsOccurrence.ContainsKey(oneLink.AbsoluteUri))
                    {
                        hrefsOccurrence[oneLink.AbsoluteUri]++;
                    }
                    else
                    {
                        hrefsOccurrence.Add(oneLink.AbsoluteUri, 1);
                    }

                    if (titlesOccurrence.ContainsKey(oneLink.Title))
                    {
                        titlesOccurrence[oneLink.Title]++;
                    }
                    else
                    {
                        titlesOccurrence.Add(oneLink.Title, 1);
                    }
                }

                postsLinks.Add(posts[i], linksFromOnePost);
            }

            HashSet<FeedItem> result = new HashSet<FeedItem>();


            //Проходим по всем постам

            foreach (var node in postsLinks)
            {
                Dictionary<Link, int> links = new Dictionary<Link, int>();
                // По каждой ссылке из каждого поста
                foreach (var link in node.Value)
                {
                    int entry = 0; //счетчик встречаемости ссылки

                    entry += hrefsOccurrence[link.AbsoluteUri];
                    entry += titlesOccurrence[link.Title];

                    if (_negativeWordsOnLink.IsMatch(link.Title))
                    {
                        entry++;
                    }

                    if (!links.ContainsKey(link))
                    {
                        links.Add(link, entry);
                    }
                }

                if (links.Count > 0)
                {
                    Link vinner = links.OrderBy(a => a.Value).First().Key;

                    FeedItem thisPost = new FeedItem() { Link = vinner, Html = node.Key};
                    if (!result.Contains(thisPost))
                    {
                        result.Add(thisPost);
                    }
                }
            }

            return result.ToList();
        }

        // Коллекция уникальных ссылок каждого поста
        internal List<Link> LinkListFromPost(HtmlNode post)
        {
            
            List<HtmlNode> linkElements = new List<HtmlNode>();
            var links = post.Descendants("a").Where(a => !String.IsNullOrWhiteSpace(a.InnerText.Trim())).ToList();
            foreach (HtmlNode item in links)
            {
                if (item.Attributes.Any(a => _positivUriWordsForCategory.IsMatch(a.Value) || _veryNegativUriWordsForPostTitle.IsMatch(a.Value))) continue;
                linkElements.Add(item);
            }

            var hrefTitleList = linkElements.Select(a => new { Href = a.GetAttributeValue("href", String.Empty), Title = LinkAnchor(a) });

            HashSet<Link> postLinks = new HashSet<Link>();
            foreach (var hrefTitle in hrefTitleList)
            {
                Uri uri = null;
                if (Uri.IsWellFormedUriString(hrefTitle.Href,UriKind.RelativeOrAbsolute) && Uri.TryCreate(hrefTitle.Href,UriKind.RelativeOrAbsolute,out uri)) {

                    Link link = null;
                    if (uri.IsAbsoluteUri)
                    {
                        link = new Link(uri.AbsoluteUri, hrefTitle.Title);
                    }
                    else
                    {
                        link = new Link(_baseHost, uri, hrefTitle.Title);
                    }

                    if (!postLinks.Contains(link))
                    {
                        postLinks.Add(link);
                    }
                    else if (postLinks.Any(a => a.Equals(link) && a.Title.Length < link.Title.Length))
                    {
                        postLinks.RemoveWhere(a => a.Equals(link) && a.Title.Length < link.Title.Length);
                        postLinks.Add(link);
                    }
                }
            }
            return postLinks.ToList();
        }

        private string LinkAnchor(HtmlNode aNode)
        {
            string title = null;

            if (aNode.ChildNodes.Count > 1) { 
                HtmlNode mayBeTitle = aNode.Descendants().FirstOrDefault(a => a.Attributes.Any(b => b.Value.Contains("title")));
                if (mayBeTitle != null && !String.IsNullOrWhiteSpace(mayBeTitle.InnerText))
                {
                    title = mayBeTitle.InnerText;
                }
            }

            if(title == null)
            {
                title = aNode.InnerText.Trim();
            }

            return title;
        }

    }
}
